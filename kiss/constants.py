"""Constant values for this package."""

# The number of pixels to connect nodes to. Nodes get connected to each
# other when their distance is under this value. Setting to a higher value will
# consequently connect nodes 'earlier' when they are farther away from each
# other.
CONNECTION_RADIUS = 30

# The hotkey to enable the kiss functionality.
KISS_HOTKEY = "A"
