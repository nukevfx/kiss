Changelog
=========

3.1.0
-----
  - Update kiss shortcut to 'A'

3.0.0
-----
  - Complete rewrite
  - Customize kiss shortcut and connection radius

2.0.0
-----
  - Connect output of selected node to other node's input

1.0.0
-----
  - Initial release
