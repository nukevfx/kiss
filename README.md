Kiss for Nuke
=============

Author
------
Simon Jokuschies
mail: info@leafpictures.de
web: https://www.leafpictures.de

Description
-----------
Connect nodes in Nuke by moving them to each other, mimicking the kiss
functionality of Autodesk Flame.

Installation
------------
Put the whole kiss folder anywhere and simply add it to Nuke's pluginPath by
adding the following line to your init.py::

    nuke.pluginAddPath("path/to/kiss_X.X.X")

Make sure to update ``path/to/kiss_X.X.X`` to your actual installation path, of
course, including the correct version number.

Additional information
----------------------
Please refer to the written documentation inside this package.
