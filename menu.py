"""Install the KeyboardObserver to trigger kiss."""

# Import third-party modules
import nuke

# Import local modules
from kiss import connection_handler
from kiss.constants import KISS_HOTKEY


def add_to_ui():
    """Initialize commands."""
    nuke.menu("Nuke").findItem("Edit").addCommand(
        "kiss", connection_handler.launch_kiss, KISS_HOTKEY)


add_to_ui()
