Kiss for Nuke
=============

Description
-----------
Connect nodes in Nuke by moving them to each other, mimicking the kiss
functionality of Autodesk Flame.

.. image:: img/connecting.gif

Compatibility
-------------
Nuke-8 and later

Linux, MacOS, Windows

Installation
------------
Put the whole kiss folder anywhere and simply add it to Nuke's pluginPath by
adding the following line to your init.py::

    nuke.pluginAddPath("path/to/kiss_X.X.X")

Make sure to update ``path/to/kiss_X.X.X`` to your actual installation path, of
course, including the correct version number.

How to use it
-------------
Press ``a`` and move nodes to each other so they get connected. Works in two
directions: When moving a node to another node's input then the selected nodes
output gets connected to the node's input. When moving a node to another node's
output then the selected node's input gets connected to the node's output.

Always connects to the next available node input index. Prevents from multiple
connections to the same node.

Customizations
--------------
kiss can be customized to your needs. Below you have some options.

Changing the hotkey to activate kiss
....................................
In order to change the hotkey that enables the kiss functionality, navigate
into the ``constants.py`` module of kiss and change the ``KISS_HOTKEY`` to your
needs.

Changing the connection radius
..............................
You can adjust the radius at which nodes get connected. Navigate into the
``constants.py`` module of kiss and change the ``CONNECTION_RADIUS`` to your
needs. If you wish to connect nodes 'earlier', i.e. when they are farther away
then increase the value. If you wish to connect only when nodes actually
overlap then decrease the value.

Author
------
Simon Jokuschies
mail: info@leafpictures.de
web: https://www.leafpictures.de

.. toctree::
   :maxdepth: 2
   :caption: Contents:

   changelog


Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
